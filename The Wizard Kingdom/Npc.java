import java.util.HashMap;
/**
 * Write a description of class Npc here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Npc
{
    private String name;
    private HashMap<String, Item> inventory;

    /**
     * Constructor for objects of class Npc.
     * a npc needs a name.
     */
    public Npc(String name)
    {
        this.name = name;
        inventory = new HashMap<>();
    }

    /**
     * list's all items in the npc's inventory.
     */
    public String listInventory()
    {
        String itemList = "";
        if(inventory.isEmpty() != true)
        {
            for(String key : inventory.keySet())
            {
                String item = inventory.get(key).fullItemDescription();
                itemList += " -" + item;
            }
        }
        else
        {
            itemList = "Nothing to sell.";
        }
        return "Npc inventory: \n" + itemList;
    }

    /**
     * returns the npc's name.
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * creates the default shop item's for a shop npc.
     */
    private void shopItems()
    {
        if(name.equals("peddler"))
        {
            addToInventory("crystal", "powerful crystal", 1, 500);
            addToInventory("axe", "two-handed axe", 5, 25);
            addToInventory("orb", "", 1, 45);
            addToInventory("robe", "", 3, 67);
            addToInventory("hood", "", 1, 23);
            addToInventory("boots", "", 2, 12);
        }
    }

    /**
     * default talk line for a npc.
     */
    public void talk()
    {
        System.out.println("Hello!!");
        System.out.println("My Name is " + name  + ", nice to meet you.");
    }

    /**
     * add's a item to the npc's inventory.
     */
    public void addToInventory(String itemName, String itemDescription, int itemWeight, int value)
    {
        inventory.put(itemName, new Item(itemName, itemDescription, itemWeight, value));
    }

    /**
     * activates the shop interface to barter with a npc.
     */
    public void shopInterface()
    {
        System.out.println("Welcome to my shop. \nThis is what i have for sale at the moment: \n");
        System.out.println(listInventory());
    }   
}

